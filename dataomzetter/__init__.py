import logging
import os
import pathlib
import mimetypes
import pandas as pd
from jinja2 import FileSystemLoader, Environment

import azure.functions as func

def render_from_template(directory, template_name, **kwargs):
    loader = FileSystemLoader(directory)
    env = Environment(loader=loader)
    template = env.get_template(template_name)
    return template.render(**kwargs)

def create_result(result, data):
    for index, row in data.iterrows():
        new_row = f"{row[2]}\t{row[3]}\n"
        logging.info(new_row)
        result = result + new_row
    return result

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')
    function_url = os.environ['APP_FUNCTION_URL']
    logging.info(function_url)
    name = req.params.get('name')
    if not name:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            name = req_body.get('name')

    result = ""
    if name:
        try:
            dfs = pd.read_html(name)
            df = dfs[0]
            result = create_result(result, df)
        except ValueError as ve:
            logging.error(ve)
            try:
                mod_data = '<table>' + name + '</table>'
                dfs = pd.read_html(mod_data)
                df = dfs[0]
                result = create_result(result, df)
            except ValueError as ve:
                b = f"{ve}can not convert: {name}"
                logging.error(b)
                result = 'geen geldige data gevonden'
            pass
    data = { 'resultaat' : result,
             'api_call': function_url}
    response = render_from_template(pathlib.Path(__file__).parent, 'form.html', **data)
    return func.HttpResponse(response, mimetype='text/html')


